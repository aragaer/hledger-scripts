#!/usr/bin/env stack
-- stack runghc --verbosity error --package hledger
-- stack runghc --verbosity error --package hledger --package hledger-lib --package text --package safe 
-- stack script --compile --resolver lts-20.13 --verbosity error --package hledger --package text
-- stack script --compile --resolver lts-20.13 --verbosity error --package hledger --package hledger-lib --package text --package safe
-- The topmost stack command above is used to run this script.
-- stack script uses released hledger, stack runghc uses local hledger source.
-- This script currently requires local hledger source, for Hledger.Cli.Script.
------------------------------------78----------------------------------------

{-# LANGUAGE OverloadedStrings, PackageImports #-}

import Hledger.Cli.Script
import Data.Decimal (roundTo)
import qualified "text" Data.Text as T
import qualified "text" Data.Text.IO as T

import qualified Data.Set as S

incomeTaxRate = 0.06 -- 6% income tax
ssExtraPayment = 0.01 -- 1% additional social security payment
ssExtraPaymentThreshold = 300000 -- from yearly income above 300000

cmdmode = hledgerCommandMode (unlines
    -- Command name, then --help text, then _FLAGS; empty help lines get stripped:
  ["taxes information for individual enterpreneur"
  ,"This is an example of a (hledger-lib-using) hledger script."
  ,"Usage: hledger-taxes [OPTS] [ARGS]"
  ,"or:    hledger taxes -- [OPTS] [ARGS]"
  ,"Examples:"
  ,"$ hledger-taxes --help"
  ,"(this help)"
    ------------------------------------78----------------------------------------
  ,""
  ,"_FLAGS"
  ])
  [] [generalflagsgroup1] [] ([], Just $ argsFlag "[ARGS]")  -- or Nothing

rub n = nullamt{aquantity=roundTo 2 n, astyle=amountstyle{asprecision=Precision 2}}

queryAccount :: Journal -> [String] -> IO Amount
queryAccount j query = do
  opts@CliOpts{reportspec_=rspec} <- getHledgerCliOpts' balancemode query
  pure $ case filteredAmounts . snd $ balanceReport rspec j of
           [] -> rub 0
           [i] -> i
  where
    filteredAmounts x = S.toList . S.map (`filterOne` x) . maCommodities $ x
    filterOne c = fromJust . unifyMixedAmount . filterMixedAmountByCommodity c

fixedSsRatio = do
  month <- getCurrentMonth
  pure $ monthToRatio month
  where
    monthToRatio m | m < 2 = 0
                   | m < 5 = 0.25
                   | m < 8 = 0.5
                   | m < 11 = 0.75
                   | otherwise = 1

main = do
  opts@CliOpts{reportspec_=rspec} <- getHledgerCliOpts cmdmode
  withJournalDo opts $ \j -> do
    taxable <- queryAccount j ["tag:taxable"]
    taxPaid <- queryAccount j ["assets:prepaid taxes"]
    taxReduction <- queryAccount j ["assets:paid fees"]
    fixedForYear <- abs <$> queryAccount j ["liabilities:fees:страховые взносы:фиксированные"]
    fixedRatio <- fixedSsRatio
    fixedPaid <- queryAccount j ["assets:paid fees:fixed"]
    variablePaid <- queryAccount j ["assets:paid fees:above 300k"]
    let taxIncurred = rub $ aquantity taxable * incomeTaxRate
        fixedIncurred = rub $ (aquantity fixedForYear) * fixedRatio
        overThreshold = max 0 $ aquantity taxable - ssExtraPaymentThreshold
        variableIncurred = rub $ overThreshold * ssExtraPayment
        fixedToPay = fixedIncurred + fixedPaid
        variableToPay = variableIncurred - variablePaid
        expectedReduction = fixedToPay + variableToPay
        taxToPay = taxIncurred - taxPaid - taxReduction - expectedReduction
    putStr $ unlines (
      [unwords ["Total taxable:", showAmount taxable]
      ,"Social Security:"
      ,"- fixed amount"
      ,unwords ["  - total:", showAmount fixedForYear]
      ,unwords ["  - incurred:", showAmount fixedIncurred]
      ,unwords ["  - paid:", showAmount fixedPaid]
      ,unwords ["  - due:", showAmount fixedToPay]
      ,"- variable amount"
      ,unwords ["  - incurred:", showAmount variableIncurred]
      ,unwords ["  - paid:", showAmount variablePaid]
      ,unwords ["  - due:", showAmount variableToPay]
      ,"Tax:"
      ,unwords ["- incurred:", showAmount taxIncurred]
      ,unwords ["- prepaid:", showAmount taxPaid]
      ,unwords ["- reduction:"
               , showAmount taxReduction
               , "+"
               , showAmount expectedReduction]
      ,unwords ["- due:", showAmount taxToPay]
      ])
